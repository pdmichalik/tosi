﻿using System;
using System.Text;
using Rsa768;

namespace ElGamal
{
    public class ElGamal
    {
        private long g;
        private long p;
        private long y;
        private long x;
        private readonly Random _random = new Random();

        public ElGamal()
        {
            p = Primes.NextBigPrime();

            //g - losowa liczba względnie pierwsza z p i mniejsza od p
            g = Primes.RelPrime(p, p);

            //y=g^x(mod p) gdzie x to losowa liczba mniejsza od p-1
            x = _random.Next((int)p - 1);
            y = Helpers.PowerModulo(g, x, p);
        }



        public bool CheckSignature(long a, long b, String hash)
        {
            long M = BitConverter.ToUInt32(Encoding.ASCII.GetBytes(hash), 0);
            //M=M mod p;
            var left = (long) Helpers.PowerModulo(y, a, p)*Helpers.PowerModulo(a,b,p)%p;
            var right = (Helpers.PowerModulo(g,M,p));;
            return left == right;
            //czy y^a * a*b = g^M mod p
        }

        public Signature CreateSignature(String hash)
        {
            long M = BitConverter.ToInt32(Encoding.ASCII.GetBytes(hash), 0);
            //M=M.mod(p);

            //k względnie pierwsza z p-1
            var k = Primes.RelPrime(p-1, p-1);
            
            long a = Helpers.PowerModulo(g, k, p); //a = gk mod p
            long t = Primes.InverseModulo(k, p);
            long b = (t*(M - x*a))%p - 1; 
            //b=t*(M-xa ) mod p-1
            
            return new Signature(){A = a, B = b};
        }

    }

    public struct Signature
    {
        public long A;
        public long B;
    }
}
