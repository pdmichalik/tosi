﻿namespace Sha1.Console
{
    public static class Program
    {
        private static void Main()
        {
            var value = "text";
            var byteblock = Sha1.GetBytes(value);

            var paddedUints = Sha1.PadInputToWords(byteblock);

            var hash = Sha1.ProcessBlock(paddedUints);
            System.Console.WriteLine("Hash of {0} is {1}", value, Sha1.UintArrayToString(hash));

            System.Console.ReadLine();
        }
    }
}

