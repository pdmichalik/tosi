﻿using System;

namespace ElGamal.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            ElGamal elGamal = new ElGamal();
            var value = "text";
            var byteblock = Sha1.Sha1.GetBytes(value);
            var paddedUints = Sha1.Sha1.PadInputToWords(byteblock);
            var hashed = Sha1.Sha1.ProcessBlock(paddedUints);

            var key = elGamal.CreateSignature(String.Join("",hashed));
            var text = elGamal.CheckSignature(key.A, key.B, String.Join("", hashed));

            System.Console.Write(text);
            System.Console.Read();
        }
    }
}
