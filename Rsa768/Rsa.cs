namespace Rsa768
{
    public class Rsa
    {
        public static RsaKeysPair GenerateRsaKey()
        {
            long euler, n;
            {
                var p = Primes.NextBigPrime();
                var q = Primes.NextBigPrime();
                euler = (p - 1)*(q - 1);
                n = p*q;
            }
            var e = Primes.RelPrime(n, euler);
            var d = Primes.InverseModulo(e, euler);

            return new RsaKeysPair
            {
                PrivateKey = new PrivateKey(d, n),//String.Format("({0},{1})", d, n),
                PublicKey = new PublicKey(e, n)// String.Format("({0},{1})", d, n)
            };
        }

        public struct RsaKeysPair
        {
            public PrivateKey PrivateKey { get; set; }
            public PublicKey PublicKey { get; set; }
        }
    }
}