namespace Rsa768
{
    public static class Helpers
    {
        public static long PowerModulo(long a, long b, long m)
        {
            long i;
            long result = 1;
            long x = a % m;

            for (i = 1; i <= b; i <<= 1)
            {
                x %= m;
                if ((b & i) != 0)
                {
                    result *= x;
                    result %= m;
                }
                x *= x;
            }

            return result;
        }
    }
}