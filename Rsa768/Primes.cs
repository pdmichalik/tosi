using System;

namespace Rsa768
{
    public static class Primes
    {
        private static readonly Random Random = new Random();
        private static bool IsPrime(long number)
        {
            var boundary = (long) Math.Floor(Math.Sqrt(number));

            if (number == 1) return false;
            if (number == 2) return true;

            for (long i = 2; i <= boundary; ++i)
            {
                if (number % i == 0) return false;
            }

            return true;
        }

        public static long NextBigPrime()
        {
            while (true)
            {
                var biglongeger = Random.Next(1000, 1999);
                if (IsPrime(biglongeger))
                    return biglongeger;
            }
        }

        private static long Nwd(long a, long b)
        {
            while (a!=b)
                if (a < b) b -= a; else a -= b;
            return a;
        }

        public static long RelPrime(long n, long euler)
        {
            for(long e = 2; e < n; ++e)
                if (Nwd(e, euler) == 1)
                    return e;
            throw new Exception("");
        }

        public static long InverseModulo(long a, long b)
        {
            long u = 1; 
            var w = a;
            long x = 0;
            var z = b;
            long q;

            while (w != 0)
            {
                if (w < z)
                {
                    q = u; u = x; x = q;
                    q = w; w = z; z = q;
                }
                q = w / z;
                u -= q * x;
                w -= q * z;
            }
            if (z == 1)
            {
                if (x < 0) x += b;
                return x;
            }
            throw new Exception("");
        }
    }
}