﻿namespace Rsa768
{
    public class PrivateKey
    {
        public readonly long _d;
        public readonly long _n;

        public PrivateKey(long d, long n)
        {
            _d = d;
            _n = n;
        }
    }
}