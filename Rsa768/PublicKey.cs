﻿namespace Rsa768
{
    public class PublicKey
    {
        public readonly long _e;
        public readonly long _n;

        public PublicKey(long e, long n)
        {
            _e = e;
            _n = n;
        }
    }
}