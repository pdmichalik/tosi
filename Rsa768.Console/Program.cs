﻿using System;

namespace Rsa768
{
    internal static class Program
    {
        private static void Main()
        {
            var rsa = new Rsa();
            var rsaKeys = Rsa.GenerateRsaKey();

            Console.WriteLine("Private: " + rsaKeys.PrivateKey._d);
            Console.WriteLine("Public: " + rsaKeys.PublicKey._e);
            Console.WriteLine("N: " + rsaKeys.PublicKey._n);

            //TEST

            var value = 111;

            long encrypted = Helpers.PowerModulo(value, rsaKeys.PublicKey._e, rsaKeys.PublicKey._n);
            Console.WriteLine("Encrypted: "+encrypted);

            long decrypted = Helpers.PowerModulo(encrypted, rsaKeys.PrivateKey._d, rsaKeys.PrivateKey._n);
            Console.WriteLine("Decrypted: " + decrypted);

            Console.Read();
        }
    }
}
   
